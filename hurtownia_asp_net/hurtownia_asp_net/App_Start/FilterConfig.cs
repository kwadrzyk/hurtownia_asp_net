﻿using System.Web;
using System.Web.Mvc;

namespace hurtownia_asp_net
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}